import string as s
from random import *

def CreatePassword():
    al = s.ascii_lowercase 
    d=  s.digits
    p = s.punctuation
    au = s.ascii_uppercase
    election = al, d, p, au
    password  = ''.join(choice(election) for x in range(1))
    return password

CreatePassword()
print(password)